# material-aulas-2022-2

[[_TOC_]]

## Conteúdos

1.	Arquitetura da web: conceitos básicos sobre redes de computadores, arquiteturas de aplicações cliente-servidor, o papel do navegador web, conceitos e exemplos de servidores web (http e de aplicação); 
2.	Protocolos de comunicação da internet: conceitos sobre protocolos de comunicação importantes para o desenvolvimento web (TCP/UPD, HTTP, FTP, SMTP e DNS);
3.	Internet e web: introdução aos conceitos de aplicações web, infraestrutura web, infraestrutura em nuvem, serviços web, URLs e domínos;
4.	Conceitos básicos de programação de computadores: programação imperativa, programação orientada a eventos, programação síncrona/assíncrona, ferramentas básicas de programação, versionamento de código (git);
5.	Fundamentos da linguagem HTML: conceito de linguagem de marcação de texto, estrutura do documento HTML, elementos de layout, elementos de layout não-semânticos, elementos de destaque de texto, elementos de navegação, mídia, formulários e tabelas;
6.	Fundamentos da linguagem CSS: seletores, herança, box model, background, bordas, cores, manipulação de texto, overflow de conteúdo, valores, unidades de medida e manipulação de imagens;
7.	Fundamentos da linguagem Javascript: gramática básica, introdução aos tipos de dados e funções, expressões e operadores;
8.	Introdução a Bancos de Dados para desenvolvimento web: conceito de bancos de dados, bancos de dados relacionais, bancos de dados NoSQL e mapeamento objeto-relacional.
9.	Desenvolvimento de páginas para internet: ferramentas de programação HTML, CSS e JavaScript; ferramentas para análise de documentos HTML e CSS; análise de sites existentes.
10.	Conceitos básicos sobre interfaces de usuário dinamicamente adaptáveis: layout baseado em proporções, layout responsivo, formatação CSS condicional com media queries.
11.	Frameworks para desenvolvimento fullstack: conceitos e exemplos de frameworks para desenvolvimento integral, ou parcial, de aplicações web em diferentes linguagens de programação; 
12.	Aplicações web com renderização no servidor: utilização de templates html e conceitos de arquitetura de sistema Model-View-Controller
13.	Desenvolvendo APIs web – conceito de interface de programação de aplicações, arquitetura de serviços e micro-serviços web, protocolos de aplicação REST e SOAP, segurança, formatos de dados, e frameworks para desenvolvimento de APIs.
14.	Consumindo e enviando dados de/para APIs web: ferramentas de consulta HTTP, consultas com segurança, consumindo/enviando dados de aplicações web. 

## exemplo HTML mínimo
```
<!DOCTYPE html>
<html>
  <head>
  <meta charset = "utf−8">
  <title> Página de Teste </title>
</head>
<body>
  <p> Página de teste</p>
</body>
</html>
```

## grupo whatsapp

<img src='QR_grupo.png' width=200>
